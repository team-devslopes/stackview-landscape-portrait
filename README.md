# Slope Session - Stack View Magic: Part 2 - Landscape & Portrait Hack #

[![stack-view-magic-part-2.png](https://bitbucket.org/repo/gjXdee/images/4141803485-stack-view-magic-part-2.png)
](https://www.youtube.com/watch?v=eyC_V4CoeJw)

*Click the image above to watch this Slope Session on YouTube.*
### Description ###

This is the source code accompanying the Stack View Magic: Part 2 - Landscape & Portrait Hack Slope Session. You will use UIStackView to create a user interface that looks amazing in *both* portrait and landscape orientations. 👍

**Note:** *Be sure to drag in '**cityart.png**' from the 'Resources' folder and into your Assets.xcassets in Xcode so that the app can successfully find and use it.*

Happy coding!

![170.png](https://bitbucket.org/repo/9LeroX/images/1216100977-170.png)

**Caleb Stultz**